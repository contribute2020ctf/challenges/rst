class ResetController < ApplicationController
  def password_reset
    @user = User.where(reset_password_token: params[:token]).first
    if @user
      session[params[:token]] = @user.id
    else
      user_id = session[params[:token]]
      @user = User.find(user_id) if user_id
    end
    if !@user
      render :plain => "no way!"
      return
    elsif params[:password] && @user && params[:password].length > 6
      @user.password = params[:password]
        if @user.save
          render :plain => "password changed ;)"
          return
        end
    end
    render :plain => "error saving password!"
  end
end
