# Stolen^WInspired by https://thoughtbot.com/blog/rails-on-docker
FROM registry.gitlab.com/contribute2020ctf/challenges/rails_base_image:latest

ADD . $APP_HOME
# RUN RAILS_ENV=production bundle exec rake assets:precompile
ARG rails_master_key
ENV RAILS_MASTER_KEY=$rails_master_key
RUN RAILS_ENV=production rails assets:precompile
RUN yarn install && yarn cache clean

EXPOSE 3000
